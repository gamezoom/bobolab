Элон Маск был назначен членом совета директоров Twitter на двухлетний срок, через день после приобретения 9,2% акций социальной медиа-компании (откроется в новой вкладке), о чем стало известно из заявления, поданного в Комиссию по ценным бумагам и биржам.

Он будет работать в качестве директора класса II в течение двух лет, а срок его полномочий закончится на ежегодном собрании акционеров компании в 2024 году. Генеральный директор Twitter Параг Агравал также подтвердил новое место Маска в совете директоров компании.

"Он одновременно и страстный верующий, и ярый критик сервиса, что как раз то, что нам нужно в Twitter и в зале заседаний совета директоров, чтобы сделать нас сильнее в долгосрочной перспективе", - добавил Агравал.

В ответ Маск сообщил о "значительных улучшениях", которые он намерен внести в социальную медиаплатформу в ближайшие месяцы.

Ранее Маск укорял Twitter за "несоблюдение принципов свободы слова". Его 9,2% акций Twitter оцениваются в 2,89 миллиарда долларов. Это дает ему право на владение 73 486 938 акциями, что делает его крупнейшим индивидуальным акционером Twitter.

Однако существуют ключевые условия, ограничивающие долю собственности Маска, чтобы предотвратить потенциальное поглощение. Согласно заявлению SEC (открывается в новой вкладке), он не может владеть более чем 15% акций компании, будучи директором совета директоров.

"Пока г-н Маск работает в совете директоров и в течение 90 дней после этого, г-н Маск не будет, ни один, ни в составе группы, становиться бенефициарным владельцем более чем 14,9% обыкновенных акций компании, находящихся в обращении на тот момент, включая для этих целей экономический риск через производные ценные бумаги, свопы или операции хеджирования", - говорится в заявлении.
https://bdroid.ru/3198-twitter-naznachit-jelona-maska-v-svoj-sovet-direktorov-s-opredelennymi-ogranichenijami.html